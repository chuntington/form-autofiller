<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
	    <form id="vaForm">
	        <div class="row">
	            <div class="col-sm-4">
	                <div class="form-group">
	                    <label for="FirstName">First Name (Full)</label>
	                    <input type="text" name="FirstName" class="form-control">
	                </div>
	            </div>
	            <div class="col-sm-4">
	                <div class="form-group">
	                    <label for="PreferredName">Preferred First Name</label>
	                    <input type="text" name="PreferredName" class="form-control">
	                </div>
	            </div>
	            <div class="col-sm-4">
	                <div class="form-group">
	                    <label for="LastName">Last Name</label>
	                    <input type="text" name="LastName" class="form-control">
	                </div>
	            </div>
	        </div>

	        <div class="row">
	            <div class="col-md-4">
	                <label>Email</label><div class="form-group multiple-form-group input-group">
	                    <div class="input-group-btn input-group-select">
	                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="select-display">Personal</span> <span class="caret"></span></button>
	                        <ul class="dropdown-menu" role="menu">
	                            <li><a href="#" data-value="2">Personal</a></li>
	                            <li><a href="#" data-value="1">Business</a></li>
	                        </ul>
	                        <input type="hidden" class="input-group-select-val" name="EmailType" value="2">
	                    </div>
	                    <input type="text" name="Email" class="form-control">
	                </div>
	            </div>
	            <div class="col-md-4">
	                <label>Phone Number</label><div class="form-group multiple-form-group input-group">
	                    <div class="input-group-btn input-group-select">
	                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="select-display">Mobile</span> <span class="caret"></span></button>
	                        <ul class="dropdown-menu" role="menu">
	                            <li><a href="#" data-value="2">Mobile</a></li>
	                            <li><a href="#" data-value="1">Home</a></li>
	                            <li><a href="#" data-value="3">Business</a></li>
	                        </ul>
	                        <input type="hidden" class="input-group-select-val" name="PhoneNumberType" value="2">
	                    </div>
	                    <input type="text" name="PhoneNumber" class="form-control">
	                </div>
	            </div>
	            <div class="col-sm-4">
	                <div class="form-group">
	                    <label for="ZipCode">Home Zip Code</label>
	                    <input type="text" name="ZipCode" class="form-control">
	                </div>
	            </div>        
	        </div>

	        <div class="row">
	            <div class="col-sm-12">
	                <div class="form-group">
	                    <label for="Message">Message</label>
	                    <textarea name="Message" class="form-control"></textarea>
	                </div>
	            </div>
	        </div>

	        <input type="submit" class="btn class btn-primary" value="Contact Us Today!">
	    </form>
	                
	    <form id="vaForm">
	        <div class="row">
	            <div class="col-sm-4">
	                <div class="form-group">
	                    <label for="FirstName">First Name (Full)</label>
	                    <input type="text" name="FirstName" class="form-control">
	                </div>
	            </div>
	            <div class="col-sm-4">
	                <div class="form-group">
	                    <label for="PreferredName">Preferred First Name</label>
	                    <input type="text" name="PreferredName" class="form-control">
	                </div>
	            </div>
	            <div class="col-sm-4">
	                <div class="form-group">
	                    <label for="LastName">Last Name</label>
	                    <input type="text" name="LastName" class="form-control">
	                </div>
	            </div>
	        </div>

	        <div class="row">
	            <div class="col-md-4">
	                <label>Email</label><div class="form-group multiple-form-group input-group">
	                    <div class="input-group-btn input-group-select">
	                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="select-display">Personal</span> <span class="caret"></span></button>
	                        <ul class="dropdown-menu" role="menu">
	                            <li><a href="#" data-value="2">Personal</a></li>
	                            <li><a href="#" data-value="1">Business</a></li>
	                        </ul>
	                        <input type="hidden" class="input-group-select-val" name="EmailType" value="2">
	                    </div>
	                    <input type="text" name="Email" class="form-control">
	                </div>
	            </div>
	            <div class="col-md-4">
	                <label>Phone Number</label><div class="form-group multiple-form-group input-group">
	                    <div class="input-group-btn input-group-select">
	                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="select-display">Mobile</span> <span class="caret"></span></button>
	                        <ul class="dropdown-menu" role="menu">
	                            <li><a href="#" data-value="2">Mobile</a></li>
	                            <li><a href="#" data-value="1">Home</a></li>
	                            <li><a href="#" data-value="3">Business</a></li>
	                        </ul>
	                        <input type="hidden" class="input-group-select-val" name="PhoneNumberType" value="2">
	                    </div>
	                    <input type="text" name="PhoneNumber" class="form-control">
	                </div>
	            </div>
	            <div class="col-sm-4">
	                <div class="form-group">
	                    <label for="ZipCode">Home Zip Code</label>
	                    <input type="text" name="ZipCode" class="form-control">
	                </div>
	            </div>        
	        </div>

	        <div class="row">
	            <div class="col-sm-12">
	                <div class="form-group">
	                    <label for="Message">Message</label>
	                    <textarea name="Message" class="form-control"></textarea>
	                </div>
	            </div>
	        </div>

	        <div class="row">
	        	<div class="col-sm-6">
	        		<div class="form-group">
	        			<label for="Location"></label>
	        			<select name="Location">
	        				<option>---</option>
	        				<option value="1">One</option>
	        				<option value="2">Two</option>
	        			</select>
	        		</div>
	        	</div>
	        	<div class="col-sm-6">
	        		<div class="form-group">
	        			<label for="Date">Date</label>
	        			<input type="date" name="Date" class="form-control">
	        		</div>
	        	</div>
	        </div>

	        <input type="submit" class="btn class btn-primary" value="Contact Us Today!">
	    </form>
	</div>
	
	<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="autofill.js"></script>
</body>
</html>