(function() {

    if (typeof jQuery=='undefined') {

        var headTag = document.getElementsByTagName("head")[0],
            jq = document.createElement('script');
        
        jq.src = 'https://code.jquery.com/jquery-1.11.3.min.js';
        jq.onload = addFillButtons;

        headTag.appendChild(jq);

    } else {

        addFillButtons();

    }

    var user = {

        FirstName:      'Cameron',
        PreferredName:  'Cam',
        LastName:       'Huntington',
        Email:          'chuntington@gradientps.com',
        PhoneNumber:    '5007272727',
        ZipCode:        '44444',
        Message:        'This is a test submission from Gradient Positioning Systems.',
        Date:           '1970-01-01'

    }

    function addFillButtons() {

        jQuery('form#vaForm').each(function(index) {
            var btn = jQuery('<button class="btn btn-block btn-danger">Autofill</button>');
            btn.on('click', function(event) {
                event.preventDefault();
                populateForm(index);
            });

            jQuery(this).attr('data-af_id', index).prepend(btn);
        });

    }

    function populateForm(formId) {

        jQuery.fn.extend({
            mark: function() {
                return this.css({'background': 'mintcream', 'color': 'black'});
            },

            populateInputs: function() {
                this.find('input')
                    .not('[type="submit"], [type="date"], [type="hidden"], .wpcf7-captchar')
                    .val('Which one of these is the non-smoking lifeboat?')
                    .mark();
                return this;
            },

            populateSelects: function() {
                var selects = this.find('select');
                selects.each(function(index) {
                    var lastOption = jQuery(this).children('option').last().val();
                    jQuery(this).val(lastOption).mark();
                });
                return this;
            },

            populateTextAreas: function() {
                this.find('textarea')
                    .val('The problem with trouble shooting is that trouble shoots back.')
                    .mark();
                return this;
            },

            populateDates: function() {
                this.find('input[type="date"]')
                    .val('1970-01-01')
                    .mark();
                return this;
            }
        });

        var vaForm = jQuery('form#vaForm[data-af_id="'+formId+'"]');

        vaForm.populateInputs()
              .populateTextAreas()
              .populateSelects()
              .populateDates();

        for (field in user) {
            vaForm.find('*[name="'+field+'"]').val(user[field]).mark();
        }

    }
})();